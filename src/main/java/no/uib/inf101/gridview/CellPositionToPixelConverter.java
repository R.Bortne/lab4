package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

//import no.uib.inf101.colorgrid.ColorGrid;
//import no.uib.inf101.colorgrid.IColorGrid;
import java.awt.Color;
import java.awt.geom.Rectangle2D;

public class CellPositionToPixelConverter {
  // TODO: Implement this class
  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition pos) {

    // double boxMargin = 20; // Avstand mellom ruten og kantene på lerretet
    // double boxX = this.getWidth() - boxWidth - boxMargin;
    // double boxY = this.getHeight() - boxHeight - boxMargin;
    // Rectangle2D box = new Rectangle2D.Double(boxX, boxY, boxWidth, boxHeight);

    int cols = gd.cols();
    int rows = gd.rows();

    double bredde = (box.getWidth() - 2 * margin - (cols-1)*margin) / cols ;
    double høyde = (box.getHeight() - 2 * margin - (rows-1)*margin) / rows;

    double x = box.getX() + margin + bredde * pos.col() + margin * pos.col();
    double y = box.getY() + margin + høyde * pos.row() + margin * pos.row();

    Rectangle2D rec = new Rectangle2D.Double(x, y, bredde, høyde);
    return rec;

  }

}
