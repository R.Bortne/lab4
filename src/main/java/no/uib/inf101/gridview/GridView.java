package no.uib.inf101.gridview;

import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicBorders.MarginBorder;

import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel{
  
  private IColorGrid grid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid grid) {
    this.setPreferredSize(new java.awt.Dimension(400, 300));
    this.grid = grid;
    
  }

  @Override
  public void paintComponent(java.awt.Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2); 
  } 
    
  private void drawGrid(Graphics2D g2) {    
    
    g2.setColor(MARGINCOLOR);
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    var rec = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, width, height);
    g2.fill(rec);

    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(rec, grid, OUTERMARGIN);
    
    
    drawCells(g2, grid ,converter);

    
    
  }

  private static void drawCells(Graphics2D g2, CellColorCollection grid2, CellPositionToPixelConverter converter) {

    var ruter = grid2.getCells();
    for (var rute : ruter) {      
      if(rute.color() == null){
        g2.setColor(Color.DARK_GRAY);
      }
      else{
        g2.setColor(rute.color());
      }
      g2.fill(converter.getBoundsForCell(rute.cellPosition()));
    }

  }


  

  

  
}
