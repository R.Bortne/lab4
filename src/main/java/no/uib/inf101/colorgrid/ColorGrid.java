package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
  int rows;
  int cols;
  CellPosition pos;
  List<CellColor> Cells = new ArrayList<CellColor>();

  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
  
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
        pos = new CellPosition(i, j);
        Cells.add(new CellColor(pos, null));
      }
    }
  }

  @Override
  public int rows() {
    // TODO Auto-generated method stub
    return this.rows;
  }

  @Override
  public int cols() {
    // TODO Auto-generated method stub
    return this.cols;
  }

  @Override
  public List<CellColor> getCells() {
    // TODO Auto-generated method stub
    return this.Cells;
  }

  @Override
  public Color get(CellPosition pos) {
    // TODO Auto-generated method stub
    if (pos.row() < 0 || pos.row() >= rows || pos.col() < 0 || pos.col() >= cols) {
      throw new IndexOutOfBoundsException();
    }
    for (CellColor cellColor : Cells) {
      if (cellColor.cellPosition().equals(pos)) {
        return cellColor.color();
      }
    }
    return null;
  }

  @Override
  public void set(CellPosition pos, Color color) {
    // TODO Auto-generated method stub
    if (pos.row() < 0 || pos.row() >= rows || pos.col() < 0 || pos.col() >= cols) {
      throw new IndexOutOfBoundsException();
    }
    for (int i = 0; i < Cells.size(); i++) {
      CellColor cellColor = Cells.get(i);
      if (cellColor.cellPosition().equals(pos)) {
        Cells.set(i, new CellColor(pos, color));
        return;
      }
    } 
    
  }
  
}
